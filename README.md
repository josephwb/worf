W.O.R.F. (Which.Open.Reading.Frame.)
==============
Tagline
---------------
"To choose a suboptimal ORF: it would... *displease* me." \*Klingon scowl\*

Overview
---------------
A lightweight program to find and translate an optimal ORF from a fasta nucleotide 
sequence, codon usage table, and (optionally) blast hit information. Capable of batch 
processing. No particular genetic code is assumed, but format of codon usage table is 
assumed to draconian degree. Ambiguous codons (i.e. containing ambiguous nucleotide(s)) 
are translated as far as possible.

Installation
---------------
The easiest way to compile is to use the Makefile, and typing the following in a unix prompt:

	make

By default, this compiles using OPENMP to enable multithreaded execution for batch analyses.
If (for some reason) you wish to turn off multithreading, recompile as:

	make clean
	make mt=F

Usage
--------------
To run, type:

	 ./worf (-f fasta | -l file_list) -c codonusage [-b blast_table] [-out outname]
	   [-np num_proc] [-x extension] [-o] [-v] [-h]

where:

'fasta' is a legitimate fasta-formatted nucleotide file containing one or more sequences.  
'file_list' is a file containing the names of fasta files to process (i.e. batch processing).  
'codonusage' file lists codon usage (from blast output). Format is strictly assumed.  
'blast_table' is a tab-delimited table containing blast hit results.  
'outname' is an optional name for the output file. Only works for single file analyses.  
'num_procs' specifies the number of processors to limit analyses to. Default is all.  
   - Only works when compiled using OPENMP (default).  
'extension' sets extension option for blast hits. 0 = none, 1 = conditional, 2 = all. Default = all.  
'-o' allow overwriting of files.  
'-v' turns on verbose reporting (not recommended).  
'-h' displays this help.  

