#ifndef DECODE_HIT_H_
#define DECODE_HIT_H_

// for sequences with significant blast hits. extend from qstart and qstop. possibly. leave extension as an option.

class DecodeHit {

	string seqName, origSeq, blastPeptide, optPeptide;
	int blastStart, blastStop, seqLength, blastLength;
	bool extendSeq;
	map <string, char> codonCodeMap;

public:
	void formatSequence ();
	string translateDownstreamToAA (string const&e);
	string getBlastAA ();

// extension beyond blast prescription:
	void extendBlast ();
	void extendUp ();
	void extendDown ();

	string getOptPeptide (); // return

	DecodeHit (vector <string> & data, vector <int> const& blastInfo, CodonUsage & usage);
	~DecodeHit () {};
};

#endif /* DECODE_HIT_H_ */
