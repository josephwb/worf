/*
 * CodonUsage.cpp
 *
 *  Created on: 2012-11-13
 *      Author: josephwb
 */

#include <iostream>
#include <sstream>
#include <vector>
#include <fstream>
#include <stdlib.h>
#include <map>
#include <algorithm>
#include <math.h>
#include <set>

using namespace std;

#include "Utilities.h"
#include "CodonUsage.h"

extern bool verbose;

/*******************
*	Codon Usage
********************/

CodonUsage::CodonUsage (string const& fileName) {
// set up hard-coded containers.
	AACodes = setAACodes ();
	ambigNucs = setAmbiguousNucleotides ();
	ambigSubsets = setAmbiguousNucleotideSubsets ();

	codonUsageFileName = fileName;
	
// read in codon usage
	getCodonUsage();
}

// TODO: figure out degeneracy. may change according to translation table used. don't assume anything.


// hmm. can probably lay off a lot of error-checking since this is a controlled pipeline.
void CodonUsage::getCodonUsage () {
	ifstream inCodons;
	inCodons.open(codonUsageFileName.c_str());
	int codonCounter = 0; // should be 64
	string line;
	vector <string> temp;
	vector <string> rows; // for determining degeneracy

	//cout << "AACodes is of length: " << AACodes.size() << endl;
	cout << endl << "Processing codon usage table..." << endl;

// Read in every non-empty (or non-whitespace) line
	while (getline(inCodons, line)) {
		if (checkWhiteSpaceOnly(line)) { // empty line
			continue;
		} else {
			temp = tokenizeString(line); // should have length of 5.
			if (temp.size() != 5) {      // wee! magic numbers! assume format stays the same...
				if (verbose) cout << "WTF?!?" << endl;
				continue;
			} else {
				rows.push_back(line);
				codonUsage[temp[1]] = castToFloat(temp[3]) / 1000;
				codonToCodeMap[temp[1]] = AACodes[temp[0]];
				if (temp[0] == "End") stopCodons.push_back(temp[1]);
				if (verbose) cout << "Codon: " << temp[1] << "; Amino acid: " << codonToCodeMap[temp[1]] << endl;
				codonCounter++;
				continue;
			}
		}
	}
	inCodons.close();

// now, consider ambiguous residues.
	addRedundancyToMap(rows);

	if (verbose) cout << "Counted " << codonCounter << " codons." << endl;

	if (codonCounter != 64) {
		cout << endl << "Didn't read in 64 codons from codon usage table. That is a catastrophic problem. Exiting." << endl;
		exit(1);
	}
	if (verbose) {
		printCodonUsage ();
		cout << endl;
	}
}

// use ambiguous residue codes to further populate codonToCodeMap.
// ugly as sin, don't want to assume any particular translation table. will clean up later.
void CodonUsage::addRedundancyToMap (vector <string> data) {
	sort(data.begin(), data.end()); // in case not already
	int length = (int)data.size();

	for (int i = 0; i < length; i++) {
		string aa = parseString (data[i], 0);
		string codon = parseString (data[i], 1);
		vector <string> codons;
		codons.push_back(codon);
		int count = 1;
		bool goOn = true;
		while (goOn && (i < length)) {
			i++;
			if (parseString (data[i], 0) == aa) { // hit
				codons.push_back(parseString (data[i], 1));
				count++;
			} else { // miss
				goOn = false;
				i--;
			}
			if (i == (length - 1)) {
				goOn = false;
			}
		}
		findSubsets (codons, aa);
	}
	//cout << endl << "codonToCodeMap is of length: " << codonToCodeMap .size() << "." << endl;
	//exit (0);
}

// finds ambiguous codons recursively
// at moment, finds all with just only variable position. need to find for > 1, but these should be rare in sequences.
// need a smarter partitioning approach
void CodonUsage::findSubsets (vector <string> & codons, string & aa) {
	bool debug = false; // temporary for working with this function
	if (verbose) debug = true;
	int count = codons.size();
	int numDiff = numDifferingPositions (codons);

	if (count > 1) {
		if (debug) {
			cout << endl << "Comparing codons:";
			for (int j = 0; j < count; j++) {
				cout << "'" << codons[j] << "'";
			}
			cout << " for amino acid '" << aa << "'." << endl;
		}
		if (numDiff == 1) { // easiest. just find ambigCode for varying position
			if (debug) cout << "Codons involve only one varying position." << endl;
			string ambigCodon = codons[0]; // just alter first in set
			int varyingPos = whichPositionVaries (codons);
			string nucs = uniqueChars (codons, varyingPos);
			char ambigCode = matchMapValue (ambigNucs, nucs); // match nucs to ambigNucs
			if (debug) cout << "Involved nucs are: " << nucs << ". Matched to ambigCode: " << ambigCode << "." << endl;
			if (!ambigSubsets.count(ambigCode)) { // does not imply other ambiguous codons
				string ambigCodon = codons[0];
				ambigCodon[varyingPos] = ambigCode;
				if (codonToCodeMap.count(ambigCodon)) {
					if (debug) cout << " *** Oops. Already have this codon mapped." << endl;
				}
				codonToCodeMap[ambigCodon] = AACodes[aa]; // add to map
				if (debug) cout << "Added to codon map: " << ambigCodon << " = " << AACodes[aa] << "." << endl;
			} else {
				string ambigCodes = ambigSubsets[ambigCode]; // this is a character string with all implied ambigCodes
				for (int j = 0; j < (int)ambigCodes.size(); j++) {
					ambigCodon[varyingPos] = ambigCodes[j];
					codonToCodeMap[ambigCodon] = AACodes[aa]; // add to map
					if (debug) cout << "Added to codon map: " << ambigCodon << " = " << AACodes[aa] << "." << endl;
				}
				if (debug) cout << "Added " << ambigCodes.size() << " ambiguous codons to map." << endl;
			}
		} else {

			if (numDiff == 2) { // check if both positions can be treated together
				if (debug) cout << "Codons involve two varying positions." << endl;
				// ambigCode needs to be the same for both. maybe?
				string temp = codons[0];
				string ambNucs;
				for (int j = 0; j < 3; j++) {
					string nucs = uniqueChars (codons, j);
					if (nucs.size() > 1) {
						char ambigCode = matchMapValue (ambigNucs, nucs);
						if (ambNucs.empty()) {
							ambNucs = ambigCode;
						} else {
							ambNucs += ambigCode;
						}
						temp[j] = ambigCode;
					}
				}
				if (ambNucs[0] == ambNucs[1]) {
					if (debug) cout << endl << "*** Both varying positions us the same code '" << ambNucs[0] << "'. Wee! ***" << endl;
					codonToCodeMap[temp] = AACodes[aa];
					if (debug) cout << "Added to codon map: " << temp << " = " << AACodes[aa] << "." << endl;
				}
			}

			for (int j = 0; j < 3; j++) { // loop over positions
				string nucs = uniqueChars (codons, j);
				if (nucs.size() > 1) { // not a constant character
		// split codons based on position-character sharing
					if (debug) cout << "Processing position #" << j << "." << endl;
					vector <string> matchPos;
					vector <string> noMatchPos;
					char first = codons[0][j];
					for (int k = 0; k < count; k++) {
						if (codons[k][j] == first) {
							matchPos.push_back(codons[k]);
						} else {
							noMatchPos.push_back(codons[k]);
						}
					}
					if (matchPos.size() > 1) findSubsets (matchPos, aa);
					if (noMatchPos.size() > 1) findSubsets (noMatchPos, aa);
				}
			}
		}
	}
}


/*
NCBI character codes:
		A  adenosine          C  cytidine             G  guanine
		T  thymidine          N  A/G/C/T (any)        U  uridine
		K  G/T (keto)         S  G/C (strong)         Y  T/C (pyrimidine)
		M  A/C (amino)        W  A/T (weak)           R  G/A (purine)
		B  G/T/C              D  G/A/T                H  A/C/T
		V  G/C/A              -  gap of indeterminate length
*/
// some are subsets of others. need to consider this...
map <char, string> CodonUsage::setAmbiguousNucleotides () {
	map <char, string> m;
	m['N'] = "ACGT";
	m['K'] = "GT";
	m['S'] = "CG";
	m['Y'] = "CT";
	m['M'] = "AC";
	m['W'] = "AT";
	m['R'] = "AG";
	m['B'] = "CGT";
	m['D'] = "AGT";
	m['H'] = "ACT";
	m['V'] = "ACG";
	return m;
}

map <char, string> CodonUsage::setAmbiguousNucleotideSubsets () {
	map <char, string> m;
	m['N'] = "BDHKMRSVWY"; // contains all the rest.
	m['B'] = "KSY";
	m['D'] = "KRW";
	m['H'] = "MWY";
	m['V'] = "MRS";
	return m;
}

// for debugging only.
void CodonUsage::printCodonUsage () {
	cout << endl << "Codon Usage:" << endl << endl;
	cout << "Codon	% Use" << endl;

	for (map <string, float>::iterator pos = codonUsage.begin(); pos != codonUsage.end(); ++pos) {
		cout << pos->first << "	" << pos->second << endl;
	}
}

void CodonUsage::printcodonToCodeMap () {
	cout << endl << "codonToCodeMap:" << endl << endl;

	for (map <string, char>::iterator pos = codonToCodeMap.begin(); pos != codonToCodeMap.end(); ++pos) {
		cout << pos->first << ": " << pos->second << endl;
	}
	cout << endl << "codonToCodeMap is of length: " << codonToCodeMap .size() << "." << endl;
}

// hardcoded AA codes
map <string, char> CodonUsage::setAACodes () {
	map <string, char> m;
	m["Ala"] = 'A';
	m["Cys"] = 'C';
	m["Asp"] = 'D';
	m["Glu"] = 'E';
	m["Phe"] = 'F';
	m["Gly"] = 'G';
	m["His"] = 'H';
	m["Ile"] = 'I';
	m["Lys"] = 'K';
	m["Leu"] = 'L';
	m["Met"] = 'M'; // start
	m["Asn"] = 'N';
	m["Pro"] = 'P';
	m["Gln"] = 'Q';
	m["Arg"] = 'R';
	m["Ser"] = 'S';
	m["Thr"] = 'T';
	m["Val"] = 'V';
	m["Trp"] = 'W';
	m["Tyr"] = 'Y';
	m["End"] = '*'; // stop
	m["Ukn"] = '?'; // where  >1 codon position contains invalid nucleotide. may not occur.
	return m;
}

