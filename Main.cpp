#include <iostream>
#include <sstream>
#include <vector>
#include <fstream>
#include <stdlib.h>
#include <map>
#include <algorithm>
#include <time.h>

using namespace std;

#include "Utilities.h"
#include "CodonUsage.h"
#include "DecodeUnknown.h"
#include "DecodeHit.h"
#include "ReadWriteData.h"

#ifdef _OPENMP
    #include <omp.h>
    #define getTime() omp_get_wtime()
#else
    #define omp_get_num_procs() 1
    #define omp_get_thread_num() 0 // don't think I need this... not using.
    #define getTime() (float)clock() / CLOCKS_PER_SEC // timing depends on serial vs. parallel
#endif

bool verbose = false; // global variable for debugging.

// version information
double version = 0.1;
string month = "February";
int year = 2013;


// blast table is present. check if sequences names match. not all will have hits.
// fasta files are small enough (~30 MB) to be read in completely, and then split up across processors for analysis.
// blastData cannot be const here. hmm. not a big deal, but i want to make sure it doesn't change.
void processWithBlastInfo (map <string, vector <int> > & blastData, CodonUsage & cUsage,
	vector <string> const& fileList, bool const& overwrite, int & numProcs) {
	int numFiles = (int)fileList.size();

	if (numFiles == 1) { // single file. assume it contains >1 sequence..
		string inFile = fileList[0];

		vector < vector <string > > seqData = readFasta(inFile);
		cout << endl << "Translating " << seqData.size() << " sequences..." << endl;

		int blast = 0, noBlast = 0;
//		int th_id = 0;
//		int i = 0;
		#pragma omp parallel num_threads(numProcs)
		{
//			th_id = omp_get_thread_num();
			#pragma omp for
			for (int i = 0; i < (int)seqData.size(); i++) {
				string name = seqData[i][0];
				if (blastData.count(name)) { // if has blast hit
					DecodeHit hit (seqData[i], blastData[name], cUsage);
					seqData[i][1] = hit.getOptPeptide();
					blast++;
				} else { // no blast hit
					DecodeUnknown noHit (seqData[i], cUsage); // error-checking now in class
					seqData[i][1] = noHit.getOptPeptide();
					noBlast++;
				}
			}
		}
		cout << "Processed " << blast << " blast hits and " << noBlast << " non-blast hits." << endl;
		string out = setOutfileName (inFile, overwrite);
		writeFasta(out, seqData);
		
	} else { // batch processing of individual files.
		string fastaFileName, outFileName;
		int blast = 0, noBlast = 0;
	// reading in files is fast.
		for (int i = 0; i < numFiles; i++) {
			fastaFileName = fileList[i];
			outFileName = setOutfileName (fastaFileName, overwrite);
			vector < vector <string > > seqData = readFasta(fastaFileName);

			#pragma omp parallel num_threads(numProcs)
			{
				#pragma omp for
				for (int i = 0; i < (int)seqData.size(); i++) {
					string name = seqData[i][0];
					if (blastData.count(name)) { // if has blast hit
						DecodeHit hit (seqData[i], blastData[name], cUsage);
						seqData[i][1] = hit.getOptPeptide();
						blast++;
					} else { // no blast hit
						DecodeUnknown noHit (seqData[i], cUsage); // error-checking now in class
						seqData[i][1] = noHit.getOptPeptide();
						noBlast++;
					}
				}
			}
			cout << "Processed " << (int)seqData.size() << " sequences." << endl;
			writeFasta(outFileName, seqData);
		}
	}
}

// no blast table present. all are 'unknown'. lower priority at the moment.
void processWithoutBlastInfo (CodonUsage & cUsage, vector <string> const& fileList,
	bool const& overwrite, int & numProcs) {
	int numFiles = (int)fileList.size();

	if (numFiles == 1) { // single file. assume it contains >1 sequence..
		string inFile = fileList[0];

		vector < vector <string > > seqData = readFasta(inFile);
		cout << endl << "Translating " << seqData.size() << " sequences..." << endl;

		int count = 0;
//		int th_id = 0;
//		int i = 0;
		#pragma omp parallel num_threads(numProcs)
		{
//			th_id = omp_get_thread_num();
			#pragma omp for
			for (int i = 0; i < (int)seqData.size(); i++) {
				string name = seqData[i][0];
				DecodeUnknown noHit (seqData[i], cUsage); // error-checking now in class
				seqData[i][1] = noHit.getOptPeptide();
				count++;
			}
		}
		cout << "Processed " << count << " sequences." << endl;
		string out = setOutfileName (inFile, overwrite);
		writeFasta(out, seqData);

	} else { // batch processing of individual files.
		string fastaFileName, outFileName;
		int count = 0;
	// reading in files is fast.
		for (int i = 0; i < numFiles; i++) {
			fastaFileName = fileList[i];
			outFileName = setOutfileName (fastaFileName, overwrite);
			vector < vector <string > > seqData = readFasta(fastaFileName);

			#pragma omp parallel num_threads(numProcs)
			{
				#pragma omp for
				for (int i = 0; i < (int)seqData.size(); i++) {
					string name = seqData[i][0];
					DecodeUnknown noHit (seqData[i], cUsage); // error-checking now in class
					seqData[i][1] = noHit.getOptPeptide();
					count++;
				}
			}
			cout << "Processed " << (int)seqData.size() << " sequences." << endl;
			writeFasta(outFileName, seqData);
		}
	}
}

int main (int argc, char *argv[]) {
	int numProcs = omp_get_num_procs();
	bool overwrite = false;
	int extend = 0; // 0 = none, 1 = conditional; 2 = all; default will eventually be 2
	string fastaFileName, codonUsageFileName, outFileName, blastTable; // filenames
	vector <string> fileList; // for batch processing
	map <string, vector<int> > blastData;
	double exeTime;
	
	processCommandLineArguments (argc, argv, fileList, codonUsageFileName, outFileName,
		blastTable, numProcs, overwrite, extend);
	reportProcessorUsage (numProcs);
	reportBlastExtensionSetting (extend);
	
// should this be mandatory?
	CodonUsage cUsage (codonUsageFileName);

	exeTime = getTime();
	
// if present, process blast data first
	if (!blastTable.empty()) {
		blastData = processBlast (blastTable, extend);
		processWithBlastInfo (blastData, cUsage, fileList, overwrite, numProcs);
	} else {
		cout << "No blast information provided." << endl;
		processWithoutBlastInfo (cUsage, fileList, overwrite, numProcs);
	}
	
	exeTime = getTime() - exeTime;
	cout << "Done. Total execution time: " << exeTime << " seconds." << endl;

	return 0;
}
