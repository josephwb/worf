#ifndef CODONUSAGE_H_
#define CODONUSAGE_H_

/*******************
*	Codon Usage
********************/
/*
Read in codon usage information from blast results. takes form (no header is in the actual file):

AmAcid  Codon      Number    /1000     Fraction
Gly     GGG      905.00       18.76        0.24
Gly     GGA      525.00       10.88        0.14
Gly     GGT      441.00        9.14        0.12
Gly     GGC     1867.00       38.70        0.50
...

Meaning of the columns:
AmAcid   -  three letter code for the amino acid designated by the codon
Codon    -  Codon sequence
Number   -  Total number of occurances of this codon in the input set
/1000    -  Number of occurances of this codon per 1000 codons in the input set
Fraction -  Fraction of occurance of this codon is used from the set of
            codons representing the same amino acid

some lines may (will?) be empty. count on this to happen.
*/


class CodonUsage {
	
	string codonUsageFileName;
	vector <string> codon; // don't need this after stored in a map.
	vector <float> fractUsage; // hmm. should this be in a map instead?!? need to tie down ordering. ***
	map <string, char> AACodes; // will be constant. make hard-coded.
	map <char, string> ambigNucs; // will be constant. make hard-coded.
	map <char, string> ambigSubsets; // also hard-coded.
	map <string, float> codonUsage;
	void findSubsets (vector <string> & codons, string & aa);
	
// temporary stuff
	vector <string> stopCodons; // don't count on code.
	
// map might change depending on the organisms involved.
	map <string, char> codonToCodeMap;
	
	friend class DecodeUnknown;
	friend class DecodeHit;
	
public:
	map <string, char> setAACodes (); // hard-coded
	map <char, string> setAmbiguousNucleotides (); // hard-coded
	map <char, string> setAmbiguousNucleotideSubsets ();
	void getCodonUsage ();
	void printCodonUsage ();
	void printcodonToCodeMap ();
	void addRedundancyToMap (vector <string> data);
	
	CodonUsage (string const&);
	~CodonUsage () {};
};


#endif /* CODONUSAGE_H_ */
