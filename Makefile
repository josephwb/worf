OBJS = Main.o CodonUsage.o DecodeUnknown.o Utilities.o ReadWriteData.o DecodeHit.o
CC = g++
CFLAGS = -Wall -c -m64 -O3 -ftree-vectorize -funroll-loops
LFLAGS = -Wall -m64

# ok, so, this is ugly, but works. for non-multithreaded, compile as: make mt=F
mt ?= T
ifeq ($(mt), T)
	CFLAGS+=-fopenmp
	LFLAGS+=-fopenmp
endif

worf: $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o worf
	
Main.o: Main.cpp
	$(CC) $(CFLAGS) Main.cpp
	
DecodeUnknown.o: DecodeUnknown.cpp DecodeUnknown.h
	$(CC) $(CFLAGS) DecodeUnknown.cpp

CodonUsage.o: CodonUsage.cpp CodonUsage.h
	$(CC) $(CFLAGS) CodonUsage.cpp

Utilities.o: Utilities.cpp Utilities.h
	$(CC) $(CFLAGS) Utilities.cpp

ReadWriteData.o: ReadWriteData.cpp ReadWriteData.h
	$(CC) $(CFLAGS) ReadWriteData.cpp

DecodeHit.o: DecodeHit.cpp DecodeHit.h
	$(CC) $(CFLAGS) DecodeHit.cpp

clean:
	rm -rf *.o worf
