#ifndef DECODE_UNKNOWN_H_
#define DECODE_UNKNOWN_H_

// find optimal ORFs and translate. for sequences without blast hits.
// individual files have their own base frequencies, but same codon usages

class DecodeUnknown {
	
	string seqName, origSeq, optPeptide;
	vector <float> scores; // 'scores' will come from some model-based method (to come)
	vector <string> ORFs, peptides; // should putative ORFs be stored?
	map <string, char> codonCodeMap;
	vector <string> stopCodons;
	
	map <char, float> baseFreqs;
	int minLength; // minimum length peptide to consider
	
public:
	void populateORFs (string const&);
	string findLongestORF (string const&);
	void getBaseFreqs (string const&);
	int getMinLength ();
	string getOptPeptide ();
	
	DecodeUnknown (vector <string> & data, CodonUsage &);
	~DecodeUnknown () {};
};

#endif /* DECODE_UNKNOWN_H_ */
