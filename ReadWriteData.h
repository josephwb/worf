#ifndef READ_DATA_H_
#define READ_DATA_H_

// functions to read input files
// process tab-delimited blast output
// columns are (no header present):
// qseqid	qlen	sseqid	slen	frames	pident	nident	length	mismatch	gapopen	qstart	qend	sstart	send	evalue	bitscore

// read in tab-delimited blast hit summary file
map <string, vector <int> > processBlast (string & infile, int const& extend);

// read in fasta file. may contain more than one sequence.
vector < vector <string > > readFasta (string & seqFileName);

// wriet results to file
void writeFasta (string & outname, vector < vector <string > > const& data);

#endif /* READ_DATA_H_ */
