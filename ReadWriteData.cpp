/*

Probably a simpler python, perl, or shell script would suffice here...
Want only first entries in file. Checks if name (first string in line) has been encountered. If not, log. If so, skip.
Does not assume input names are sorted, but assumes first entry of name is the best entry (e.g. sorted by score).

assume column ordering (although no header present. I don't like this; if format changes, code breaks. column name is stable):
qseqid	qlen	sseqid	slen	frames	pident	nident	length	mismatch	gapopen	qstart	qend	sstart	send	evalue	bitscore

Interested here in columns 10 + 11 (counting from 0)
*/

#include <iostream>
#include <fstream>
#include <map>
#include <time.h>
#include <vector>
#include <stdlib.h>

using namespace std;

#include "Utilities.h"
#include "ReadWriteData.h"

extern bool verbose;

/*
return map: string(name), vector(start, stop, extend);
'extend' is essentially a boolean value for downstream.
  - either turned on/off directly, or (eventually) by some information in blast file.
currently only take first (best) hit, but this will be made more general.
*/
map <string, vector <int> > processBlast (string & infile, int const& extend) {
	string line, name, previousName;
	map <string, vector <int> > blastData;
	int lineCount = 0;
	//int extension = 0; // will be zero or one after processing. may be different from individual records.
	clock_t t = 0.0;
	
	ifstream inStream;
	
	inStream.open(infile.c_str());
	if (inStream.fail()) { // file opens without problem
		cout << endl << "Error: cannot open blast file '" << infile << "'. Exiting." << endl << endl;
		exit(0);
	}
	if (verbose) t = clock();
	
	cout << "Processing blast output, with extension option #" << extend << "..." << endl;

	while (getline(inStream, line)) { // process file
		lineCount++;
		name = parseString(line, 0);
		if (name == previousName) { // fastest option for sorted input
			continue;
		} else if (blastData.count(name)) { // in case input is not sorted
			previousName = name;
			continue;
		} else {
			previousName = name;
			blastData[name].push_back( castToInt(parseString(line, 10)) ); // start
			blastData[name].push_back( castToInt(parseString(line, 11)) ); // stop
			if (extend != 0) { // extension option. work to be done here regarding conditional extensions.
				blastData[name].push_back(1);
			} else {
				blastData[name].push_back(0);
			}
		}
	}
	inStream.close();
	if (verbose) t = clock() - t;
	
	cout << blastData.size() << " unique records (of " << lineCount << " original read)." << endl;
	if (verbose) cout << "Total execution time: " << ((float)t)/CLOCKS_PER_SEC << " seconds." << endl;
	
	return blastData;
}

// format variability should be handled (e.g. single vs. multiple line sequences
vector < vector <string > > readFasta (string & seqFileName) {
	vector < vector <string > > data; // name, sequence
	vector <string> temp;
	int numChar = 0, seqCount = 0;
	string seqName, sequence, line;
	bool seqStart = false;

	cout << endl << "Reading fasta sequences from file '" << seqFileName << "'." << endl;

	ifstream inData;
	inData.open(seqFileName.c_str());

// Read in every non-empty (or non-whitespace) line
	while (getline(inData, line)) {
		if (checkWhiteSpaceOnly(line)) { // empty line. shouldn't happen (right?)
			continue;
		} else {
			if (line[0] == '>') {
				if (seqStart) { // log previous sequence
					sequence = stringToUpper(sequence); // determine if this is necessary
					temp.push_back(seqName);
					temp.push_back(sequence);
					data.push_back(temp);
					temp.clear();
				}
				seqName = line;
				seqName.erase(seqName.begin()); // get rid of '>'
				seqStart = false;
				seqCount++;
				continue;
			} else {
				if (seqStart) { // second or greater line
					sequence += line;
				} else {       // first sequence line
					sequence = line;
					seqStart = true;
				}
				numChar += (int)line.size();
				continue;
			}
		}
	}
// log last
	sequence = stringToUpper(sequence); // determine if this is necessary
	temp.push_back(seqName);
	temp.push_back(sequence);
	data.push_back(temp);

	inData.close();

	cout << seqCount << " sequences read." << endl;
	return data;
}

void writeFasta (string & outname, vector < vector <string > > const& data) {
	ofstream outFile;
	outFile.open(outname.c_str());
// currently prints antire sequence in one line. should this be 50 char/line instead? ask Ya.
	for (int i = 0; i < (int)data.size(); i++) {
		outFile << ">" << data[i][0] << endl;
		outFile << data[i][1] << endl;
	}
	cout << "Wrote " << (int)data.size() << " record to file '" << outname << "'." << endl << endl;
}
