/*
 * Utilities.cpp
 *
 *  Created on: 2012-10-31
 *      Author: josephwb
 */

#include <iostream>
#include <sstream>
#include <vector>
#include <fstream>
#include <stdlib.h>
#include <map>
#include <algorithm>

using namespace std;

#include "Utilities.h"

#ifdef _OPENMP
	#include <omp.h>
	#define multiThread true
#else
	#define multiThread false
#endif

extern bool verbose;

extern double version;
extern string month;
extern int year;

void printVersionInfo () {
	cout
	<< "WORF version " << version << " (" << month << ", " << year << ")" << endl
	<< endl;
}

void printProgramUsage () {
	cout << "Usage:" << endl
	<< endl
	<< "   ./worf (-f fasta | -l file_list) -c codonusage [-b blast_table] [-out outname] [-np num_proc] [-x extension] [-o] [-v] [-h]" << endl
	<< endl
	<< "where:" << endl
	<< endl
	<< "   'fasta' is a legitimate fasta-formatted nucleotide file containing one sequence." << endl
	<< "   'file_list' is a file containing the names of fasta files to process (i.e. batch processing)."  << endl
	<< "   'codonusage' file lists codon usage (from blast output). Format is strictly assumed." << endl
	<< "   'blast_table' is a tab-delimited table containing blast hit results." << endl
	<< "   'outname' is an optional name for the output file. Only works for single file analyses." << endl
	<< "   'num_procs' specifies the number of processors to limit analyses to. Default is all." << endl
	<< "      - Only works when compiled using OPENMP (default)."<< endl
	<< "   'extension' sets extension option for blast hits. 0 = none, 1 = conditional, 2 = all. Default = all." << endl
	<< "   '-o' allow overwriting of files." << endl
	<< "   '-v' turns on verbose reporting (not recommended)." << endl
	<< "   '-h' displays this help." << endl
	<< endl;
}

void complainAndExit () {
	printVersionInfo();
	cout << "Error: missing expected arguments." << endl << endl;
	printProgramUsage();
	cout << "Exiting." << endl << endl;
	exit(1);
}

// read in commandline arguments, check file validity.
void processCommandLineArguments(int argc, char *argv[], vector <string> & fileList,
	string & codonUsageFileName, string & outFileName, string & blastTable,
	int & numProcs, bool & overwrite, int & extend) {
	
	if (argc == 1) {
		complainAndExit();
	} else if ((string)argv[1] == "-h" || (string)argv[1] == "-help") {
		printVersionInfo();
		cout
		<< "Program description: Find optimal ORF and translate to peptide." << endl
		<< endl;
		printProgramUsage();
		exit(0);
	} else if (argc < 5) {
		complainAndExit();
	} else {
		printVersionInfo();
		for (int i = 1; i < argc; i++) {
			string temp = argv[i];

			if (temp == "-f") { // single file
				i++;
				checkValidInputFile(argv[i]);
				fileList.push_back(argv[i]);
				continue;
			} else if (temp == "-l") { // list of files to process
				i++;
				fileList = populateFileList(argv[i]);
				continue;
			} else if (temp == "-c") {
				i++;
				checkValidInputFile(argv[i]);
				codonUsageFileName = argv[i]; // perhaps there is a default name for this? ask Ya.
				continue;
			} else if (temp == "-out") {
				i++;
				outFileName = argv[i]; // probably never used
				continue;
			} else if (temp == "-b") {
				i++;
				blastTable = argv[i];
				continue;
			} else if (temp == "-x") { // eventually will do a nicer test here, allow user to fix
				i++;
				extend = castToInt(argv[i]);
				if (extend == 0) {
					cout << "Extension for sequences with blast hits is turned off." << endl;
				}
				if (extend < 0 || extend > 2) {
					cout << endl << "Invalid extension option. Must be in 0:2. Exiting." << endl << endl;
					printProgramUsage();
					exit(0);
				}
				continue;
			} else if (temp == "-o") {
				overwrite = true;
				cout << "File overwriting is turned on." << endl;
				continue;
			} else if (temp == "-v") {
				verbose = true; // probably never used
				cout << "Turning on verbose output." << endl;
				continue;
			} else if (temp == "-np") { // warn if not compiled using OPENMP
				i++;
				if (multiThread) {
					numProcs = castToInt(argv[i]);
				} else {
					cout << "Cannot set number of processors. Recompile using OPENMP." << endl;
				}
				continue;
			} else {
				cout
				<< "Unknown command-line argument '" << argv[i] << "' encountered." << endl
				<< endl;
				printProgramUsage();
				exit(1);
			}
			cout << endl;
		}
	}
	if (verbose && numProcs > 1) {
		cout << "Verbose output is garbled with parallel execution. Setting number of threads to 1." << endl << endl;
		numProcs = 1;
	}
	if (fileList.empty()) {
		cout << "No fasta file name information given." << endl << endl;
		printProgramUsage();
		cout << endl << "Exiting." << endl;
		exit(1);
	}
	if (codonUsageFileName.empty()) {
		cout << "Missing codon usage file name." << endl << endl;
		printProgramUsage();
		cout << endl << "Exiting." << endl;
		exit(1);
	}
}

// this probably will not get as used as I first figured.
vector <string> populateFileList(string const& fileName) {
	vector <string> seqFileNames;
	checkValidInputFile(fileName);
	
	ifstream inNames;
	inNames.open(fileName.c_str());
	string line;
	int numFiles = 0;

// Read in every non-empty (or non-whitespace) line
	while (getline(inNames, line)) {
		if (checkWhiteSpaceOnly(line)) {
			continue;
		} else {
			checkValidInputFile(line); // exits if invalid file. hmm, should this be here? test.
			seqFileNames.push_back(line);
			numFiles++;
		}
	}
	inNames.close();

	if (verbose) cout << "Counted " << numFiles << " valid files to process." << endl;

	return seqFileNames;
}

// set name for output peptide file from input fasta name if no output name provided.
string setOutfileName (string const& fastaFileName, bool const& overwrite) {
	string outFileName = removeStringSuffix(fastaFileName) + ".pep";
	checkValidOutputFile(outFileName, overwrite);
	return outFileName;
}

// * Error-checking functions * //
// - many of these can probably be done without. should expect little variability.

void reportProcessorUsage (int const& numProcs) {
	if (numProcs > 1) {
		cout << numProcs << " threads available for analysis." << endl;
	} else {
		cout << numProcs << " thread available for analysis." << endl;
	}
}

void reportBlastExtensionSetting (int const& extend) {
	if (extend == 0) {
		cout << "Blast extension is turned off." << endl;
	} else if (extend == 1) {
		cout << "Blast extension is set to 'conditional' (conditions not yet implemented. set to 'on')." << endl;
	} else {
		cout << "Blast extension is turned on." << endl;
	}
}

// check that input files exist and can be opened.
bool checkValidInputFile (string const& fileName) {
	bool validInput = false;
	ifstream tempStream;

	tempStream.open(fileName.c_str());
	if (tempStream.fail()) {
		ofstream errorReport("Error.WORF.txt");
		errorReport << "WORF analysis failed." << endl << "Error: unable to open file '";
		errorReport << fileName << "'" << endl;
		errorReport.close();

		cerr << endl << "WORF analysis failed. " << endl << "Error: unable to open file '";
		cerr << fileName << "'" <<  endl;
		exit(1);
	} else {
		if (verbose) {
			cout << "Successfully opened file '" << fileName << "'."<< endl;
			tempStream.close();
			tempStream.clear();
		}
	}
	return validInput;
}

// check that output filename can be opened.
bool checkValidOutputFile (string & outputFileName, bool const& overwrite) {
	bool testOutBool = true;
	bool fileNameAcceptable = false;
	bool keepFileName = false;

// First, check if file already exists, so overwriting can be prevented
	fstream testIn;
	while (!fileNameAcceptable) {
		testIn.open(outputFileName.c_str());
		if (!testIn) {
			testIn.close();
			fileNameAcceptable = true;
		} else {
			testIn.close();
			if (!overwrite) {
				cout << endl << "Default output file '" << outputFileName << "' exists!  Change name (0) or overwrite (1)? ";
				cin >> keepFileName;
				if (!keepFileName) {
					cout << "Enter new output file name: ";
					cin >> outputFileName;
				} else {
					cout << "Overwriting existing file '" << outputFileName << "'." << endl;
					fileNameAcceptable = true;
				}
			} else {
				cout << "Overwriting existing file '" << outputFileName << "'." << endl;
				fileNameAcceptable = true;
			}
		}
	}

// check outfile can be opened.
	ofstream outFile;
	outFile.open(outputFileName.c_str());

	if (outFile.fail()) {
		ofstream errorReport("Error.WORF.log");
		errorReport << "WORF analysis failed." << endl << "Error: unable to open file '";
		errorReport << outputFileName << "'" << endl;
		errorReport.close();

		cerr << endl << "WORF analysis failed." << endl << "Error: unable to open file '";
		cerr << outputFileName << "'" <<  endl;
		testOutBool = false;
		exit(1);
	} else {
		outFile.close();
		outFile.clear();
	}
	return testOutBool;
}

// remove file suffix (extension). will use last "." in filename (if present). otherwise, string is returned unaltered.
// this is expected to be ".fas", but may not be(?). could be a problem if no extension but "." in filename.
string removeStringSuffix (string const& stringToParse) {
	string temp;
	vector <char> tempVector;
	int charCounter = 0, suffixStart = 0;
	bool delimiterEncountered = false;
	char delimiter = '.';

	for (string::const_iterator charIter = stringToParse.begin(); charIter < stringToParse.end(); charIter++ ) {
		tempVector.push_back(*charIter);
		if (*charIter == delimiter) {
			suffixStart = charCounter;
			delimiterEncountered = true;
		}
		charCounter++;
	}
	if (delimiterEncountered) {
		for (int charIter = 0; charIter < suffixStart; charIter++) {
			temp += tempVector[charIter];
		}
	} else {
		temp = stringToParse;
	}
	return temp;
}

bool checkWhiteSpaceOnly (string const& stringToParse) {
	bool whiteSpaceOnly = true;

// empty line
	if (stringToParse.empty()) {
		return whiteSpaceOnly;
	}

// contains only tabs or spaces
	vector <string> tempVector;
	istringstream tempStream(stringToParse);
	string tempString;
	while (tempStream >> tempString) {
		if (tempString != "	" && tempString != " ") {
			whiteSpaceOnly = false;
		}
	}
	return whiteSpaceOnly;
}

float castToFloat (string const& stringToConvert) {
	float temp = 0.0;
	istringstream tempStream(stringToConvert);
	tempStream >> temp;

	return temp;
}

int castToInt (string const& stringToConvert) {
	int temp = 0.0;
	istringstream tempStream(stringToConvert);
	tempStream >> temp;
	
	return temp;
}

vector <string> tokenizeString (string const& stringToParse) {
	vector <string> tempVector;
	istringstream tempStream(stringToParse);
	string tempString;
	while (tempStream >> tempString) {
		tempVector.push_back(tempString);
	}
	return tempVector;
}

string stringToUpper (string & stringToConvert) {
// explicit cast needed to resolve ambiguity
	std::transform(stringToConvert.begin(), stringToConvert.end(), stringToConvert.begin(), (int(*)(int)) std::toupper);
	return stringToConvert;
}

string parseString (string stringToParse, int const& stringPosition) {
	string temp;
	int counter = 0;
	istringstream tempStream(stringToParse);
	while (tempStream >> temp) {
		if (counter == stringPosition) {
			break;
		}
		counter++;
	}
	return temp;
}

// flip sequence and get complement
string reverseComplement (string const& forwardSequence) {
	string reverseComplement = forwardSequence;
	reverse(reverseComplement.begin(), reverseComplement.end());

	for (int i = 0; i < (int)reverseComplement.size(); i++) {
		reverseComplement[i] = complement(reverseComplement[i]);
	}

	return reverseComplement;
}

// capital characters are ensured at data read-in. need to incorporate ambiguous sites.
inline char complement (char const& nuc) {
	char original = nuc;
	char complement;

	if (original == 'A') {
		complement = 'T';
	} else if (original == 'T') {
		complement = 'A';
	} else if (original == 'C') {
		complement = 'G';
	} else if (original == 'G') {
		complement = 'C';
	} else if (original == 'N') {
		complement = original;
	} else if (original == 'K') {
		complement = 'M';
	} else if (original == 'M') {
		complement = 'K';
	} else if (original == 'S') {
		complement = original;
	} else if (original == 'Y') {
		complement = 'R';
	} else if (original == 'R') {
		complement = 'Y';
	} else if (original == 'W') {
		complement = original;
	} else if (original == 'B') {
		complement = 'V';
	} else if (original == 'V') {
		complement = 'B';
	} else if (original == 'D') {
		complement = 'H';
	} else if (original == 'H') {
		complement = 'D';
	} else {
		complement = original; // e.g. character = '?'
		cout << "Hit ambiguous residue '" << original << "'." << endl;
	}
	return complement;
}





// ugh. there's got to be existing tools to do this string comparison stuff. clean up later.

// find which positions in a suite of strings vary. this version assumes just one difference.
int whichPositionVaries (vector <string> const& stringsToCompare) {
	int pos = -1;
	int numPositions = (int)stringsToCompare[0].size();
	for (int i = 0; i < numPositions; i++) {
		string temp = uniqueChars (stringsToCompare, i);
		if (temp.size() > 1) {
			pos = i;
			break;
		}
	}

	return pos;
}

// find the number of positions within constant-length strings that exhibit polymorphism
int numDifferingPositions (vector <string> const& stringsToCompare) {
	int numDiff = 0;
	int numPositions = (int)stringsToCompare[0].size();
	for (int i = 0; i < numPositions; i++) {
		string temp = uniqueChars (stringsToCompare, i);
		if (temp.size() > 1) numDiff++;
	}
	return numDiff;
}

// find all unique characters at some position across all strings.
string uniqueChars (vector <string> const& stringsToCompare, int const& position) {
	char a = stringsToCompare[0][position];
	string diff (1, a);
	for (int i = 1; i < (int)stringsToCompare.size(); i++) {
		char nuc = stringsToCompare[i][position];
		bool match = false;
		for (int j = 0; j < (int)diff.size(); j++) {
			if (nuc == diff[j]) {
				match = true;
				break;
			}
		}
		if (!match) { // new character
			diff += nuc;
		}
	}
	return diff;
}

// opposite use of map. don't feel like making the reverse map. assumes only one hit.
char matchMapValue (map <char, string> mapToSearch, string stringToMatch) {
	char res = '?';
	for (map <char, string>::iterator pos = mapToSearch.begin(); pos != mapToSearch.end(); ++pos) {
		if (pos->second == stringToMatch) {
			res = pos->first;
			break;
		}
	}
	return res;
}

// conserve order within character sets (codons; groupSize = 3), but reverse the order of the sets.
// allows using downstream translation machinery to translate upstream sequences without messing up reading frame
string reverseCharSets (string const& stringToChange, int const& groupSize) {
	string res;
	int length = (int)stringToChange.size();
	for (int i = (length - 1); i > 0; i-=groupSize) { // do in reverse
		string temp = stringToChange.substr((i - groupSize + 1), groupSize);
//		cout << "temp = " << temp << ", start = " << (i - groupSize + 1) << ", stop = " << i << "." << endl;
		if (i == (length - 1)) {
			res = temp;
		} else {
			res += temp;
		}
	}
//	cout << "Converted '" << stringToChange << "' to '" << res << "'." << endl;

	return res;
}
