#include <iostream>
#include <sstream>
#include <vector>
#include <fstream>
#include <stdlib.h>
#include <map>
#include <algorithm>
#include <math.h>

using namespace std;

#include "Utilities.h"
#include "CodonUsage.h"
#include "DecodeHit.h"

extern bool verbose;

DecodeHit::DecodeHit (vector <string> & data, vector <int> const& blastInfo, CodonUsage & usage)
: blastStart(0), blastStop(0), seqLength(0), extendSeq(true)
{
	seqName = data[0];
	origSeq = data[1];
	seqLength = (int)origSeq.size();
	codonCodeMap = usage.codonToCodeMap;

// -1 because blast nucleotide counts start at 1 and not zero
	blastStart = blastInfo[0] - 1;
	blastStop = blastInfo[1] - 1;
	blastLength = abs(blastStop - blastStart) + 1;
	extendSeq = blastInfo[2]; // should extension be attempted?

// reverse sequence and renumber, if necessary
	formatSequence ();

// range that blast identifies
	blastPeptide = getBlastAA();

	if (!extendSeq) { // no extension. return blast-identified region
		optPeptide = blastPeptide;
	} else { // try to extend in both directions
		extendBlast ();
		optPeptide = blastPeptide;
	}
}

// return the peptide already!
string DecodeHit::getOptPeptide () {
	return optPeptide;
}

// if reverse direction, get reverse complement, and alter start/stop
void DecodeHit::formatSequence () {
// reverse direction. flip it, renumber range.
// note start/stop have already been put into zero-start counting system. beware one-off errors!
	if (blastStop < blastStart) {
		origSeq = reverseComplement(origSeq);
	// new start/stop
		blastStart = seqLength - blastStart - 1;
		blastStop = blastStart + blastLength - 1;
	}
}

// translate region blast identifies.
// in the case where extension = false, this is what is returned.
string DecodeHit::getBlastAA () {
	string seq, prot;
	seq = origSeq.substr(blastStart, ((blastStop - blastStart + 1)));
	prot = translateDownstreamToAA(seq);
	return prot;
}

// translate all, downstream.
string DecodeHit::translateDownstreamToAA (string const& seq) {
	string protein;
	char aa;
	int residue = 0;
	int length = seq.size();
	
	if (length % 3 != 0) cout << "Um, length is not a multiple of 3. That shouldn't occur from blast data (I think...)" << endl;

	map <string, char>::iterator mapIter;
	
	for (int i = 0; i < length; i+=3) {
		residue++;
		mapIter = codonCodeMap.find(seq.substr(i, 3));
		if (mapIter == codonCodeMap.end()) {
			if (verbose) cout << "Hit unmappable codon at residue '" << seq.substr(i, 3) << "'." << endl;
			aa = '?';
		} else {
			aa = (*mapIter).second;
		}
		protein += aa;
	}
//	cout << "Translated protein has " << residue << " amino acid residues." << endl;
	return protein;
}

void DecodeHit::extendBlast () {
// first, downstream. check if last character of blastPeptide is stop codon. if so, do not extend downstream. check length of remaining sequence.
	if (*blastPeptide.rbegin() != '*' && ((seqLength - (blastStop + 1)) >= 3)) {
		extendDown ();
	}
// now, upstream. check length of upstream sequence.
	if ((blastStart - 1) >= 2) {
		extendUp ();
	}
}

void DecodeHit::extendUp () {
	string upSeq = origSeq.substr(0, blastStart); // grab all flanking downstream residues
	string upPep;

// trim lead characters so length is a multiple of 3. implicitly keeps frame constant.
	int leadingChars = upSeq.size() % 3;
	if (leadingChars > 0) {
		upSeq.erase(upSeq.begin(), upSeq.begin() + leadingChars);
	}

// reverse order of codons (but not codons themselves) to allow use of translateDownstreamToAA
// NOT reverse complement
// should be faster than translating everything and processing later, as early exit is possible.
	upSeq = reverseCharSets (upSeq, 3);

	if (upSeq.size() > 2) { // must be long enough to form an aa
		upPep = translateDownstreamToAA (upSeq);
// NOTE: amino acids are in REVERSE order here. process left to right, then flip.
		int startPos = 0, stopPos = 0;
		vector <int> unknownPos;
		bool startFound = false, stopFound = false, unknownFound = false;
		for (int i = 0; i < (int)upPep.size(); i++) {
			if (upPep[i] == '*') { // found stop. that's no good... stop on first.
				stopPos = i;
				stopFound = true;
				break;
			} else if (upPep[i] == '?') {
				unknownPos.push_back(i);
				unknownFound = true; // continue? what to do?
			} else if (upPep[i] == 'M') { // will continue upstream. may hit another.
				startPos = i;
				startFound = true; // continue? what to do?
			}
		}

// TODO: set this up as explicit cases

// case 1: extended all the way to the 'end' (beginning). start is upstream of sample. add all.
		if (!startFound && !stopFound && !unknownFound) {
			reverse(upPep.begin(), upPep.end());
//			cout << "Valid upstream sequence extends beyond sampling. Adding all (" << upPep.size() << ") to peptide." << endl;
//			cout << "Adding string '" << upPep << "' (length = " << upPep.size() << ") to start of blastPep (currently of length = "
//				<< blastPeptide.size() << "). ";
			blastPeptide = upPep + blastPeptide;
//			cout << "blastPeptide is now of size: " << blastPeptide.size() << "." << endl;
		} else if (startFound && stopFound) {
//			cout << "Found both start (" << startPos << ") and stop (" << stopPos << ") upstream." << endl;
// case 2: start and stop are upstream. stop is further. add up to and including start.
			if (stopPos > startPos) { // valid
				upPep = upPep.substr(0, startPos + 1);
				reverse(upPep.begin(), upPep.end());
//				cout << "Valid upstream start codon exists, downstream of stop. Adding (" << upPep.size() << ") to peptide." << endl;
//				cout << "\t" << upPep << endl;
				blastPeptide = upPep + blastPeptide;
			} else {
				cout << "*** Um, shouldn't be here..." << endl;
			}
		}
	} else {
		cout << "*** Oops. Shouldn't be here... ***" << endl;
	}
}

void DecodeHit::extendDown () {
	string downSeq = origSeq.substr((blastStop + 1)); // grab all flanking downstream residues
	string downPep;

// trim last characters so length is a multiple of 3
	int trailingChars = downSeq.size() % 3;
	if (trailingChars > 0) {
		downSeq.erase((downSeq.end() - trailingChars), downSeq.end());
	}

	if (downSeq.size() > 2) { // must be long enough to form an aa
		downPep = translateDownstreamToAA (downSeq);
		int stopPos = 0;
		vector <int> unknownPos;
		bool stopFound = false, unknownFound = false;
		for (int i = 0; i < (int)downPep.size(); i++) {
			if (downPep[i] == '*') { // found stop. add to blastPeptide.
				stopPos = i;
//				cout << "Found a stop codon downstream from blastPeptide! Wee! Add to existing peptide. ";
				stopFound = true;
				break;
			} else if (downPep[i] == '?') {
//				cout << "*** Could not translate current codon. What do you want to do now? ***" << endl;
				unknownPos.push_back(i);
				unknownFound = true; // continue? what to do?
			}
		}
		if (stopFound) {
			downPep = downPep.substr(0, stopPos + 1);
//			cout << "Adding string '" << downPep << "' (length = " << downPep.size() << ") to end of blastPep (currently of length = "
//				<< blastPeptide.size() << ")." << endl;
			blastPeptide += downPep;
//			cout << "blastPeptide is now of size: " << blastPeptide.size() << "." << endl;
		} else {
			if (!unknownFound) { // interpretation here is sequence ends past what was sampled. Everything is cool.
//				cout << "Adding string '" << downPep << "' (length = " << downPep.size() << "; without 'stop') to end of blastPep (currently of length = "
//					<< blastPeptide.size() << ")." << endl;
				blastPeptide += downPep;
			}
		}
	} else {
		cout << "*** Oops. Shouldn't be here... ***" << endl;
	}
}
