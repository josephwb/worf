#include <iostream>
#include <sstream>
#include <vector>
#include <fstream>
#include <stdlib.h>
#include <map>
#include <algorithm>
#include <math.h>

using namespace std;

#include "Utilities.h"
#include "CodonUsage.h"
#include "DecodeUnknown.h"

extern bool verbose;

DecodeUnknown::DecodeUnknown (vector <string> & data, CodonUsage & usage)
: scores(6, 0.0), ORFs(6, ""), peptides(6, "")
{
	seqName = data[0];
	origSeq = data[1];

	codonCodeMap = usage.codonToCodeMap;
	stopCodons = usage.stopCodons;

// get baseFreqs for determining minimum length of acceptable peptides
	getBaseFreqs (origSeq);

// minLength based on geometric probability of seeing a length of random sequence that does not possess a stop codon
	minLength = getMinLength ();

// get all 6 potential ORFs
	populateORFs (origSeq);

	int longest = 0;
	int best = 0; // this is for tagging the name with the inferred frame.
	for (int i = 0; i < (int)ORFs.size(); i++) {
		peptides[i] = findLongestORF(ORFs[i]);
		if ((int)peptides[i].size() > longest) {
			longest = (int)peptides[i].size();
			optPeptide = peptides[i];
			best = i;
		} else if ((int)peptides[i].size() == longest) { // check if tie. go with whichever (if any) has a valid start.
			if (optPeptide[0] != 'M' && peptides[i][0] == 'M') {
				optPeptide = peptides[i]; // replace
			}
		}
	}
// hmm. if lengths are similar, should we go with one that has a valid start?
	if (verbose) {
		cout << endl << "*** Sizes of optimal ORFs for sequence '" << seqName << "' (best is #" << best << "): ***" << endl;
		for (int i = 0; i < (int)peptides.size(); i++) {
			cout << i << "): " << peptides[i].size() << endl;
		}
		cout << endl;
		if (longest < minLength) {
			cout << "Longest ORF from sequence '" << seqName << "' = " << longest
				<< "; but minimum calculated length = " << minLength << ". That is not good..." << endl;
		}
	}
	//seqName = seqName + "__"; // perhaps eventually tag name with inferred frame (maybe)...
}

// return the peptide already!
string DecodeUnknown::getOptPeptide () {
	return optPeptide;
}

// get all 6 possible ORFs. update this function.
void DecodeUnknown::populateORFs (string const& originalSequence) {
	string forward = originalSequence;
	string reverse = reverseComplement(originalSequence);
	
	ORFs[0] = forward;
	ORFs[1] = forward.erase(0, 1);
	ORFs[2] = forward.erase(0, 1);
	ORFs[3] = reverse;
	ORFs[4] = reverse.erase(0, 1);
	ORFs[5] = reverse.erase(0, 1);
}

/* pass in nucleotide sequence; return longest/best contiguous protein for assumed ORF.

 In any potential reading frame, 5 scenarios possible:
 1) coding region begins upstream of first nucleotide in sequence, ends within sequence.
 	 - no start codon is valid. need stop codon.
 	 - inferred peptide must begin at start of observed sequence.
 2) coding region ends downstream of last nucleotide, starts within sequence.
 	 - no stop codon is valid. need start codon.
 	 - inferred peptide must terminate at end of observed sequence.
 3) entire coding region is within nucleotide sequence. 
 	 - need both start and stop codons.
 4) coding region begins upstream and ends downstream of nucleotide sequence.
 	 - no start or stop codons is valid.
 	 - inferred peptide must begin at start of observed sequence.
 5) last codon is incomplete. this does not factor in with +=3 iterator

Sequence:        |--------------------------------------------|
1.  Start------------------Stop
2.                                               Start-------------------Stop
3.                      Start-----------------Stop
4.  Start----------------------------------------------------------------Stop

TODO: keep track of codon usage frequency. not of immediate interest.
TODO: calculate lower bound on considered protein length
TODO: ignore proteins without a valid start

*/
string DecodeUnknown::findLongestORF (string const& seq) {
	string protein, temp;
	char aa;
	
// just for debugging
	vector <string> validPolypeptides;
	
	map <string, char>::iterator mapIter;

	int start = 0, best = 0, residue = -1;
	int currStart = 0, currLength = 0;
	int seqLength = (int)seq.size();
	bool validStart = false;
	
	for (int i = 0; i < seqLength; i+=3) {
		residue++;
		mapIter = codonCodeMap.find(seq.substr(i, 3));
		if (mapIter == codonCodeMap.end()) {
			if (verbose) cout << "Hit unmappable codon at residue #" << residue << "." << endl;
			aa = '?'; // stop reading?
		} else {
			aa = (*mapIter).second;
		}
		protein += aa; // store all "aa", even *, extract on completion
		if (currLength == 0) {
			currStart = residue;
			if (aa == 'M' || currStart == 0) {
				validStart = true;
			} else {
				validStart = false;
			}
		}
		if (aa == '*' || (i > seqLength - 4)) { // terminate current polypeptide
			currLength += 1; // want to include stop in translated protein
			if (validStart) {
				validPolypeptides.push_back(protein.substr(currStart, currLength));
				if (currLength > best) {
					best = currLength;
					start = currStart;
//					if (currStart == 0) { // first upstream sequence considered
//						if (verbose) {
//							cout << "First sequence (of length " << best
//								<< "; start=" << start << ", stop=" << start+best-1
//								<< "): " << protein.substr(start, best) << endl;
//						}
//					} else {
//						if (verbose) {
//							cout << "Found new winner (of length " << best
//								<< "; start=" << start << ", stop=" << start+best-1
//								<< "): " << protein.substr(start, best) << endl;
//						}
//					}
				} else if (currLength == best) { // ties previous best
// check if previous winner has valid start. if no, this is the new winner.
					if (protein[start] != 'M') {
						best = currLength;
						start = currStart;
//						if (verbose) {
//							cout << "Found replacement polypeptide with valid start which ties best (of length " << currLength
//								<< "; start=" << currStart << ", stop=" << currStart+currLength-1
//								<< "): " << validPolypeptides[validPolypeptides.size() - 1] << endl;
//						}
					}
				} else { // valid polypeptide, but shorter than current best
					if (verbose) {
						cout << "Found suboptimal polypeptide (of length " << currLength
						<< "; start=" << currStart << ", stop=" << currStart+currLength-1
						<< "): " << protein.substr(currStart, currLength) << endl;
					}
				}
			}
			currLength = 0;
		} else {
			currLength += 1;
		}
	}
	if (verbose) {
		cout << endl << "Translated protein (total " << residue << " residues): " << protein << endl;
		cout << "Found " << validPolypeptides.size() << " valid polypeptides in this ORF." << endl;
		cout << "Longest polypeptide in this ORF (" << best << " residues; positions ["
			<< start << "," << start+best << "]: " << protein.substr(start, best) << endl;
	}
	protein = protein.substr(start, best);
	return protein;
}

// compute base frequencies. consider both strands. only used in calculating minLength.
void DecodeUnknown::getBaseFreqs (string const& sequence) {
	char nuc;
	float a = 0, c = 0, g = 0, t = 0, sum = 0;
	for (int i = 0; i < (int)sequence.size(); i++) {
		nuc = sequence[i];
		sum += 2;
		if (nuc == 'A') {
			a++; t++;
		} else if (nuc == 'C') {
			c++; g++;
		} else if (nuc == 'G') {
			g++; c++;
		} else if (nuc == 'T') {
			t++; a++;
		} else { // invalid nucleotide
			sum -= 2;
		}
	}
	baseFreqs['A'] = (a / sum);
	baseFreqs['C'] = (c / sum);
	baseFreqs['G'] = (g / sum);
	baseFreqs['T'] = (t / sum);
}

// take in stop codons, calculate random sequence frequencies
// use geometric distribution to find longest 'valid' random sequence expected given empirical baseFreqs
// should we just have a hard minimum instead?
int DecodeUnknown::getMinLength () {
	int minLength = 0;
	float threshold = 0.95;

// p is the probability of getting a stop codon by chance.
// equal to the sum of the product of codon-specific nucleotide frequencies.
	float p = 0.0;

	for (int i = 0; i < (int)stopCodons.size(); i++) {
		string stop = stopCodons[i];
		float temp = 1.0;
		for (int j = 0; j < 3; j++) {
			temp *= baseFreqs[ stop[j] ];
		}
		p += temp;
	}

// R: getK <- function (p, threshold=0.95) floor(log(1-threshold) / log(1-p));
// cdf for geometric distribution = 1 - (1-p)^k
// where k is the trial number of the first success (here: produce a random stop codon of any flavour)
	minLength = floor((log(1-threshold) / log(1-p)));

	return minLength;
}

// R code:
//cumGeom <- function (p, threshold=0.95) {
//	val <- 0;
//	k <- 1;
//	while (val < threshold) {
//		val <- (1 - (1-p)^k);
//		k <- k + 1;
//		cat("val =", val, "\n")
//	}
//	return (k);
//}
//
//getProb <- function (p, k) (1 - (1-p)^k);
//getK <- function (p, threshold=0.95) floor(log(1-threshold) / log(1-p));
//cumGeom <- function (p, threshold=0.95) {
//	val <- 0;
//	k <- 0;
//	while (val < threshold) {
//		k <- k + 1;
//		val <- (1 - (1-p)^k);
//		cat(k, ". val = ", val, "\n", sep="");
//	}
//	return (k);
//}
