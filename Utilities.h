#ifndef UTILITIES_H_
#define UTILITIES_H_

void printVersionInfo ();
void printProgramUsage ();
void complainAndExit ();
void processCommandLineArguments (int argc, char *argv[], vector <string> & fileList,
	string & codonUsageFileName, string & outFileName, string & blastTable,
	int & numProcs, bool & overwrite, int & extend);
vector <string> populateFileList (string const& fileName);
string setOutfileName (string const& fastaFileName, bool const& overwrite);
void reportProcessorUsage(int const& numProcs);
void reportBlastExtensionSetting (int const& extend);

// utility functions
vector <string> tokenizeString (string const& stringToParse);
float castToFloat (string const& stringToConvert);
int castToInt (string const& stringToConvert);
void setOutfileName (string const& fastaFileName, string & outFileName);
bool checkValidInputFile (string const& fileName);
string removeStringSuffix (string const& stringToParse);
bool checkValidOutputFile (string & outputFileName, bool const& overwrite);
bool checkWhiteSpaceOnly (string const& stringToParse);
string stringToUpper(string & stringToConvert);
string parseString (string stringToParse, int const& stringPosition);
int whichPositionVaries (vector <string> const& stringsToCompare);
int numDifferingPositions (vector <string> const& stringsToCompare);
string uniqueChars (vector <string> const& stringsToCompare, int const& position);

string reverseComplement (string const& forwardSequence);
char complement (char const& nuc);

char matchMapValue (map <char, string> mapToSearch, string stringToMatch);
string reverseCharSets (string const& stringToChange, int const& groupSize);

#endif /* UTILITIES_H_ */
